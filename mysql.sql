-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `SequelizeMeta`
--

DROP TABLE IF EXISTS `SequelizeMeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SequelizeMeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `SequelizeMeta_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SequelizeMeta`
--

LOCK TABLES `SequelizeMeta` WRITE;
/*!40000 ALTER TABLE `SequelizeMeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `SequelizeMeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guest_group`
--

DROP TABLE IF EXISTS `guest_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guest_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `party_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT '',
  PRIMARY KEY (`group_id`,`user_id`,`party_id`),
  KEY `user_id` (`user_id`),
  KEY `party_id` (`party_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guest_group`
--

LOCK TABLES `guest_group` WRITE;
/*!40000 ALTER TABLE `guest_group` DISABLE KEYS */;
INSERT INTO `guest_group` VALUES (3,1,8,'nha trai'),(2,1,8,'nha gai'),(4,1,8,'chung');
/*!40000 ALTER TABLE `guest_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guest_party`
--

DROP TABLE IF EXISTS `guest_party`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guest_party` (
  `guest_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `response_status` int(11) DEFAULT '0',
  `guest_code` varchar(10) NOT NULL DEFAULT '',
  `party_code` varchar(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT 'guest name',
  `Address` varchar(100) DEFAULT '',
  `relationship` varchar(100) DEFAULT '',
  `phone` varchar(10) DEFAULT '',
  `facebook` varchar(100) DEFAULT '',
  `google` varchar(100) DEFAULT '',
  `twister` varchar(100) DEFAULT '',
  `invited_to_shower` tinyint(4) DEFAULT '0',
  `seat_table` int(11) DEFAULT '0',
  `notes` mediumtext,
  `qr_path` varchar(100) DEFAULT NULL,
  `party_group` int(11) DEFAULT '0',
  `qr` varchar(200) DEFAULT '',
  `party_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`guest_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guest_party`
--

LOCK TABLES `guest_party` WRITE;
/*!40000 ALTER TABLE `guest_party` DISABLE KEYS */;
INSERT INTO `guest_party` VALUES (1,1,1,'NT5XXS','I1QWWR','Anh bí','123 chat ham st','','','','','',0,2,'they need to eat meat',NULL,4,'',8),(2,1,2,'Q91A6P','I1QWWR','anh ha','456','','','','','',0,3,'eefaefae fae feefefef',NULL,2,'',8),(3,1,0,'NTOL61','I1QWWR','Howard efef','efefefefe','','','','','',0,1,'fefe',NULL,2,'',8),(4,1,1,'4DB3BW','I1QWWR','Thinh and your lovely wife','wat the fuck','','','','','',0,2,'nho',NULL,3,'',8),(5,1,1,'MDHP7B','I1QWWR','Howard Cameron','yuyuyuyuyuy','','','','','',0,3,'eefaefae fae feefefef',NULL,2,'',8),(6,1,0,'QRJ82O','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,1,'eefaefae fae feefefef',NULL,3,'',8),(7,1,0,'UP70TC','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,2,'eefaefae fae feefefefe',NULL,3,'',8),(8,1,0,'17DFWI','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,1,'fefefe',NULL,3,'',8),(9,1,0,'LXA54F','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,3,'mew',NULL,3,'',8),(10,1,0,'Y0ADYE','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,2,'eefaefae fae feefefef',NULL,3,'',8),(11,1,0,'W9LIGK','I1QWWR','cáº­p nháº­t hÃ ng giá».','wat the fuck','','','','','',0,2,'eefaefae fae feefefef',NULL,3,'',8),(12,1,1,'NB4EHJ','I1QWWR','Nguyen Phan','wat the fuck efef','','','','','',0,3,'eefaefae fae feefefef',NULL,2,'',8),(13,1,0,'',NULL,'efe','fefe','','','','','',0,1,'efefe',NULL,3,'',NULL),(27,1,0,'',NULL,'4444','5454','','','','','',0,2,'44',NULL,3,'',NULL),(28,1,0,'',NULL,'4444','5454','','','','','',0,2,'44',NULL,3,'',NULL),(29,1,0,'',NULL,'4444','5454','','','','','',0,2,'44',NULL,3,'',NULL),(30,1,0,'',NULL,'4444','5454','','','','','',0,2,'44',NULL,3,'',NULL),(31,1,0,'',NULL,'efefe','efef','','','','','',0,1,'efefe',NULL,2,'',NULL),(32,1,0,'',NULL,'dddd','dddd','','','','','',0,1,'ddd',NULL,2,'',NULL),(33,1,0,'',NULL,'efefefefef','efefe','','','','','',0,2,'effefe',NULL,2,'',NULL),(34,1,0,'',NULL,'efefef','efefe','','','','','',0,2,'efefefe',NULL,3,'',NULL),(35,1,0,'',NULL,'efefefe','efefefe','','','','','',0,2,'efefef',NULL,2,'',NULL),(36,1,0,'',NULL,'efefe','khongbiet','','','','','',0,2,'efefef',NULL,2,'',NULL),(37,1,0,'',NULL,'vutest1','vutest2','','','','','',0,1,'test1',NULL,3,'',NULL),(38,1,0,'',NULL,'vutes2','update1','','','','','',0,1,'te13',NULL,3,'',NULL),(39,1,0,'',NULL,'bentest2','update2','','','','','',0,1,'bentest2',NULL,2,'',NULL),(40,1,0,'',NULL,'bắt đầu lên kế hoạch 2','efefe','','','','','',0,1,'efefe',NULL,3,'',NULL),(41,1,1,'NT5XXS','I1QWWR',' bắt đầu lên kế hoạch ','123 chat ham st','','','','','',0,1,NULL,NULL,2,'',8),(42,1,2,'Q91A6P','I1QWWR','bắt đầu lên kế hoạch 2','456','','','','','',0,1,NULL,NULL,2,'',8),(43,1,0,'NTOL61','I1QWWR','Howard efef','efefefefe','','','','','',0,1,NULL,NULL,2,'',8),(44,1,1,'4DB3BW','I1QWWR','Thinh and your lovely wife','wat the fuck','','','','','',0,2,NULL,NULL,3,'',8),(45,1,1,'MDHP7B','I1QWWR','Howard Cameron','yuyuyuyuyuy','','','','','',0,3,NULL,NULL,2,'',8),(46,1,0,'QRJ82O','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,1,NULL,NULL,3,'',8),(47,1,0,'UP70TC','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,2,NULL,NULL,3,'',8),(48,1,0,'17DFWI','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,1,NULL,NULL,3,'',8),(49,1,0,'LXA54F','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,3,NULL,NULL,3,'',8),(50,1,0,'Y0ADYE','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,2,NULL,NULL,3,'',8),(51,1,0,'W9LIGK','I1QWWR','cáº­p nháº­t hÃ ng giá».','wat the fuck','','','','','',0,2,NULL,NULL,3,'',8),(52,1,1,'NB4EHJ','I1QWWR','Nguyen Phan','wat the fuck efef','','','','','',0,3,NULL,NULL,2,'',8),(53,1,0,'',NULL,'efe','fefe','','','','','',0,1,NULL,NULL,3,'',NULL),(54,1,0,'',NULL,'4444','5454','','','','','',0,2,NULL,NULL,3,'',NULL),(55,1,0,'',NULL,'4444','5454','','','','','',0,2,NULL,NULL,3,'',NULL),(56,1,0,'',NULL,'4444','5454','','','','','',0,2,NULL,NULL,3,'',NULL),(57,1,0,'',NULL,'4444','5454','','','','','',0,2,NULL,NULL,3,'',NULL),(58,1,0,'',NULL,'efefe','efef','','','','','',0,1,NULL,NULL,2,'',NULL),(59,1,0,'',NULL,'dddd','dddd','','','','','',0,1,NULL,NULL,2,'',NULL),(60,1,0,'',NULL,'efefefefef','efefe','','','','','',0,2,NULL,NULL,2,'',NULL),(61,1,0,'',NULL,'efefef','efefe','','','','','',0,2,NULL,NULL,3,'',NULL),(62,1,0,'',NULL,'efefefe','efefefe','','','','','',0,2,NULL,NULL,2,'',NULL),(63,1,0,'',NULL,'efefe','khongbiet','','','','','',0,2,NULL,NULL,2,'',NULL),(64,1,0,'',NULL,'vutest1','vutest2','','','','','',0,1,NULL,NULL,3,'',NULL),(65,1,0,'',NULL,'vutes2','update1','','','','','',0,1,NULL,NULL,3,'',NULL),(66,1,0,'',NULL,'bentest2','update2','','','','','',0,1,NULL,NULL,2,'',NULL),(67,1,0,'',NULL,'bắt đầu lên kế hoạch 2','efefe','','','','','',0,1,NULL,NULL,3,'',NULL),(72,1,1,'NT5XXS','I1QWWR',' bắt đầu lên kế hoạch ','123 chat ham st','','','','','',0,1,NULL,NULL,2,'',8),(73,1,2,'Q91A6P','I1QWWR','bắt đầu lên kế hoạch 2','456','','','','','',0,1,NULL,NULL,2,'',8),(74,1,0,'NTOL61','I1QWWR','Howard efef','efefefefe','','','','','',0,1,NULL,NULL,2,'',8),(75,1,1,'4DB3BW','I1QWWR','Thinh and your lovely wife','wat the fuck','','','','','',0,2,NULL,NULL,3,'',8),(76,1,1,'MDHP7B','I1QWWR','Howard Cameron','yuyuyuyuyuy','','','','','',0,3,NULL,NULL,2,'',8),(77,1,0,'QRJ82O','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,1,NULL,NULL,3,'',8),(78,1,0,'UP70TC','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,2,NULL,NULL,3,'',8),(79,1,0,'17DFWI','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,1,NULL,NULL,3,'',8),(80,1,0,'LXA54F','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,3,NULL,NULL,3,'',8),(81,1,0,'Y0ADYE','I1QWWR','Howard Cameron','wat the fuck','','','','','',0,2,NULL,NULL,3,'',8),(82,1,0,'W9LIGK','I1QWWR','cáº­p nháº­t hÃ ng giá».','wat the fuck','','','','','',0,2,NULL,NULL,3,'',8),(83,1,1,'NB4EHJ','I1QWWR','Nguyen Phan','wat the fuck efef','','','','','',0,3,NULL,NULL,2,'',8),(84,1,0,'',NULL,'efe','fefe','','','','','',0,1,NULL,NULL,3,'',NULL),(85,1,0,'',NULL,'4444','5454','','','','','',0,2,NULL,NULL,3,'',NULL),(86,1,0,'',NULL,'4444','5454','','','','','',0,2,NULL,NULL,3,'',NULL),(87,1,0,'',NULL,'4444','5454','','','','','',0,2,NULL,NULL,3,'',NULL),(88,1,0,'',NULL,'4444','5454','','','','','',0,2,NULL,NULL,3,'',NULL),(89,1,0,'',NULL,'efefe','efef','','','','','',0,1,NULL,NULL,2,'',NULL),(90,1,0,'',NULL,'dddd','dddd','','','','','',0,1,NULL,NULL,2,'',NULL),(91,1,0,'',NULL,'efefefefef','efefe','','','','','',0,2,NULL,NULL,2,'',NULL),(92,1,0,'',NULL,'efefef','efefe','','','','','',0,2,NULL,NULL,3,'',NULL),(93,1,0,'',NULL,'efefefe','efefefe','','','','','',0,2,NULL,NULL,2,'',NULL),(94,1,0,'',NULL,'efefe','khongbiet','','','','','',0,2,NULL,NULL,2,'',NULL),(95,1,0,'',NULL,'vutest1','vutest2','','','','','',0,1,NULL,NULL,3,'',NULL),(96,1,0,'',NULL,'vutes2','update1','','','','','',0,1,NULL,NULL,3,'',NULL),(97,1,0,'',NULL,'bentest2','update2','','','','','',0,1,NULL,NULL,2,'',NULL),(98,1,0,'',NULL,'bắt đầu lên kế hoạch 2','efefe','','','','','',0,1,NULL,NULL,3,'',NULL),(99,1,1,'NT5XXS','I1QWWR',' bắt đầu lên kế hoạch ','123 chat ham st','','','','','',0,1,NULL,NULL,2,'',8),(100,1,2,'Q91A6P','I1QWWR','bắt đầu lên kế hoạch 2','456','','','','','',0,1,NULL,NULL,2,'',8);
/*!40000 ALTER TABLE `guest_party` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `name_vn` varchar(50) DEFAULT NULL,
  `price` decimal(5,2) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `description1` varchar(500) DEFAULT NULL,
  `description2` varchar(500) DEFAULT NULL,
  `description3` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `fk_store_id` (`store_id`),
  CONSTRAINT `fk_store_id` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (7,'waefefawef','',11.00,2,NULL,NULL,NULL),(8,'waefefawef',NULL,11.00,2,NULL,NULL,NULL),(9,'khongco',NULL,100.00,2,NULL,NULL,NULL),(10,'12323',NULL,123.00,2,NULL,NULL,NULL),(11,'Paraffin Manicure','',16.00,15,'Appx 20min total','Do you want an extra care for your hands? This pampering service is the great choice to soften and smoothe skin. This warm wax paraffin procedure deeply moisturize your skin, improve blood circulation and bring relaxation to your hands. Extremely beneficial for arthritis and aching jointst. ','Paraffin, Organic Lotion, Hot Towel, Massage, Choice Of Polish'),(12,'Hollywood Spa Manicure','',26.00,15,'10min massage, appx 30min total','With this manicure, dead skin cells on your hands will be exfoliated with tropical sugar scrub. Your hands will be continuously pampered with paraffin treatment, tropical mask, and hot towel massage to lock in moisture. Finished with lavender or green tea organic lotion and extensive 10 min massage. Your true healthy hand will be revealed. ','Tropical Sugar Scrub, Paraffin, Lavender Organic Lotion, Hot Towel, 10min Massage, Choice Of Polish.'),(13,'Gel Manicure','',35.00,15,'5min massage, appx 30min total','With regular polish, the top coat begins to dull after a few days, but with Gel, they look as shiny on day 14 as they did on day 1. This hand treatment includes every step of a paraffin manicure finished with your choice of gel color.',''),(14,'do some thing',NULL,100.00,17,NULL,NULL,NULL),(15,'efaf','efe',111.00,17,'aef','ewaff','eawf'),(16,'Deluxe Spa Pedicure',NULL,16.00,15,'30min massage, appx 50min total','Exfoliated with sea salt scrub, callus removal, ginger scrub, ginger mask, and wrapped with warm towels to remove of dry skin. Paraffin treatment & lotion massage will deeply moisturize your skin, improve blood circulation to renew and revive your feet. Choose your choice of scent: Green Tea or Lavender.','Disposable Liner, Tropical Scrub, Callus Removal, Mint Mask, Paraffin Treatment, Organic Lotion, Hot Towel, 30min Extensive Massage, Choice Of Polish.'),(17,'awefewaf',NULL,11.00,18,'wefaf','ewaf','weaff'),(18,'wefaf',NULL,123.00,15,'waefa','ewafw','ewafawf');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seat_table`
--

DROP TABLE IF EXISTS `seat_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seat_table` (
  `table_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `party_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT '',
  `max_seat` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`table_id`),
  KEY `user_id` (`user_id`),
  KEY `party_id` (`party_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seat_table`
--

LOCK TABLES `seat_table` WRITE;
/*!40000 ALTER TABLE `seat_table` DISABLE KEYS */;
INSERT INTO `seat_table` VALUES (1,1,8,'big party',10),(2,1,8,'small table',10),(3,1,8,'main tablew',10);
/*!40000 ALTER TABLE `seat_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session_items`
--

DROP TABLE IF EXISTS `session_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_completed` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `session_items_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `store_sessions` (`session_id`),
  CONSTRAINT `session_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session_items`
--

LOCK TABLES `session_items` WRITE;
/*!40000 ALTER TABLE `session_items` DISABLE KEYS */;
INSERT INTO `session_items` VALUES (1,1,12,'2017-12-12 00:18:02',0),(2,1,13,'2017-12-12 01:13:56',0),(3,1,13,'2017-12-12 01:17:12',0);
/*!40000 ALTER TABLE `session_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `session_id` varchar(128) NOT NULL,
  `expires` int(11) unsigned NOT NULL,
  `data` text,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('3lBU8O4DDskhNSJRihS4eg7sV8Wfzh76',1515258671,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{},\"user\":{\"user_name\":\"phanb\",\"password\":\"*6691484EA6B50DDDE1926A220DA01FA9E575C18A\",\"social\":\"none\",\"first_name\":\"ben\",\"last_name\":\"phan\",\"created_at\":\"2017-12-11T04:29:56.000Z\",\"last_login\":null,\"last_login_from\":null,\"email\":\"ben\",\"is_activated\":\"0\",\"activation_code\":null,\"reset_token\":null,\"reset_expires\":null,\"user_id\":5}}'),('ui4knoeK_-bQERBiHAbbwP4KlvgFgDqH',1515258739,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{}}'),('Dj2nJ7zOGOYV6naC6FXnoSu0eFnQ0H4A',1515109776,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}'),('5C3XCYSFCxxGKHmaYuLQPDUIyHE-VEoO',1515114686,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"}}');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_sessions`
--

DROP TABLE IF EXISTS `store_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_sessions` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `customer_name` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_completed` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `store_id` (`store_id`),
  CONSTRAINT `store_sessions_ibfk_1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_sessions`
--

LOCK TABLES `store_sessions` WRITE;
/*!40000 ALTER TABLE `store_sessions` DISABLE KEYS */;
INSERT INTO `store_sessions` VALUES (1,15,'giang the bos','2017-12-11 23:39:19',0);
/*!40000 ALTER TABLE `store_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `url` varchar(100) DEFAULT NULL,
  `name2` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `address1` varchar(100) DEFAULT NULL,
  `address2` varchar(100) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `hours` varchar(100) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `zip` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores`
--

LOCK TABLES `stores` WRITE;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` VALUES (2,'pink nail',NULL,'name2','2017-12-08 02:51:01','123 kink','arlington',NULL,'703-456 9893','1-7',NULL,NULL),(3,'ben-store2',NULL,'second store','2017-12-08 04:16:57','awefawf','awef',NULL,'waefawfefef','awefaf',NULL,NULL),(15,'Best VA Nail',NULL,'one of the best nail shop at arlington','2017-12-11 05:46:27','123 king st','arlington, VA 22151',NULL,'571-232 9872','Mon-Tues: 10:00am-8:00pm | Wed-Fri: 10:00am-10:00pm |Sat: 9:00am-10:00pm |  Sun: 10:30am-7:00pm',NULL,NULL),(16,'nail for you',NULL,'nail test','2017-12-11 05:50:01','7790 abc king','arlingon',NULL,'70377649973','1-2',NULL,NULL),(17,'a nail shop',NULL,'somethign cool','2017-12-11 18:41:19','123 abc str','fairfax va 11911',NULL,'888-999-777','sunday: 1-9pm',NULL,NULL),(18,'salon 1',NULL,'best salon in VA','2017-12-11 21:13:00','123 king st','fairfax va 11122',NULL,'787 332 2343','Mon-Tues: 10:00am-8:00pm <br>Wed-Fri: 10:00am-10:00pm Sat: 9:00am-10:00pm Sun: 10:30am-7:00pm',NULL,NULL),(19,'Tinhbene',NULL,'aefaf','2017-12-17 01:56:42','123 kingst','falls church','eee','571 27 87983','awefwaf','Delaware','2222'),(25,'',NULL,NULL,'2017-12-28 19:40:27',NULL,NULL,NULL,'3434343434',NULL,NULL,NULL),(34,'Nail For You',NULL,'123','2018-01-01 23:00:08','123','123','springfield','123',NULL,'Alask','333');
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teches`
--

DROP TABLE IF EXISTS `teches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teches` (
  `tech_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `user_name` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(100) DEFAULT NULL,
  `avartar_path` varchar(30) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `hours` varchar(50) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`tech_id`),
  KEY `store_id` (`store_id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `teches_ibfk_1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`),
  CONSTRAINT `teches_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teches`
--

LOCK TABLES `teches` WRITE;
/*!40000 ALTER TABLE `teches` DISABLE KEYS */;
INSERT INTO `teches` VALUES (11,2,'eaf',NULL,'','2017-12-10 04:56:48',1,'fewaf','eaf','eaf',NULL,NULL),(12,3,'eaf',NULL,'','2017-12-10 05:01:51',1,'eaf','eaf','eaf',NULL,NULL),(45,2,'techben','*A07D1F4559117D2D13C8FB495C7B5153EF65DC91','','2017-12-10 19:36:19',1,'khongco','khongco','khongco',NULL,NULL),(49,17,'eafe','*2D93E652915BBCCDF999EB8E84203425DAC950FE','','2017-12-11 18:58:16',8,'aefa','fef','efef',NULL,NULL),(50,18,'abc123','*6691484EA6B50DDDE1926A220DA01FA9E575C18A','','2017-12-11 21:14:07',9,'88877887','uu@gmail.com','12',NULL,NULL),(57,15,'benphan','*1556A6F65259CE3FBEA8489096F7797B4E0D5BEC','','2017-12-29 16:44:09',1,'7034658617','uu@gmail.com',NULL,'beneee','phan'),(58,15,'giangdinh','*1556A6F65259CE3FBEA8489096F7797B4E0D5BEC','','2017-12-29 17:05:59',1,'5763877827','AAA@gmail.com',NULL,'giang','dinhphan'),(77,19,'weafawef','*AF0144E7AA92A0885FD581BB15AE8F0AC0723557','','2018-01-01 23:47:45',1,'7034658797','khongco@gmail.com',NULL,'ben','nguyen'),(79,19,'ngh2','*9F6BC0ACD66A76DA54DC51E104F875A17F871EB3','','2018-01-03 02:16:03',1,'89898993','abc@yahoo.com',NULL,'nguyen','nguyen'),(80,34,'phanb','*9F6BC0ACD66A76DA54DC51E104F875A17F871EB3','','2018-01-03 03:28:07',1,'573054545','nho@gmail.com',NULL,'nhoe','nho');
/*!40000 ALTER TABLE `teches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_party`
--

DROP TABLE IF EXISTS `user_party`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_party` (
  `party_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `party_name` varchar(100) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `address` varchar(300) DEFAULT '',
  `description` varchar(300) DEFAULT '',
  `times` datetime DEFAULT NULL,
  `party_time` datetime DEFAULT NULL,
  `party_date` datetime DEFAULT NULL,
  PRIMARY KEY (`party_id`,`user_id`,`party_name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_party`
--

LOCK TABLES `user_party` WRITE;
/*!40000 ALTER TABLE `user_party` DISABLE KEYS */;
INSERT INTO `user_party` VALUES (8,1,'Cu Bin 18th Birthday party :)','I1QWWR','','',NULL,NULL,NULL),(9,1,'Cu Bin 18th Birthday party :)','IARUZ8','','',NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_party` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_stores`
--

DROP TABLE IF EXISTS `user_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_stores` (
  `store_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`store_id`,`user_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_stores_ibfk_1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`store_id`),
  CONSTRAINT `user_stores_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_stores`
--

LOCK TABLES `user_stores` WRITE;
/*!40000 ALTER TABLE `user_stores` DISABLE KEYS */;
INSERT INTO `user_stores` VALUES (15,1),(19,1),(34,1),(16,6),(17,8),(18,9);
/*!40000 ALTER TABLE `user_stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(100) DEFAULT NULL,
  `social` varchar(30) DEFAULT 'none',
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime DEFAULT NULL,
  `last_login_from` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `is_activated` varchar(4) NOT NULL DEFAULT '0',
  `activation_code` varchar(30) DEFAULT NULL,
  `reset_expires` datetime DEFAULT NULL,
  `reset_token` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'phanbe','*0D3CED9BEC10A777AEC23CCC353A8C08A633045E','fb','ben','Phan','2017-12-11 03:24:59','2017-12-10 22:27:57','192.134.2.123',NULL,'0',NULL,NULL,NULL),(2,'dan','*6691484EA6B50DDDE1926A220DA01FA9E575C18A','gg','ben','Phan','2017-12-11 03:24:59','2017-12-10 22:27:57','192.134.2.123',NULL,'0',NULL,NULL,NULL),(3,'fd','*619B035D208746C080A1CDB4F0A3439A5A1AC38C','none','fiang','did','2017-12-11 04:26:35',NULL,NULL,'ben@yy.co','0',NULL,NULL,NULL),(4,'kn@gmail.com','*9F6BC0ACD66A76DA54DC51E104F875A17F871EB3','none','nguyen','khanh','2017-12-11 04:28:34',NULL,NULL,'abc@gmail.com','0',NULL,NULL,NULL),(5,'phanb','*6691484EA6B50DDDE1926A220DA01FA9E575C18A','none','ben','phan','2017-12-11 04:29:56',NULL,NULL,'ben','0',NULL,NULL,NULL),(6,'trinhle','*6691484EA6B50DDDE1926A220DA01FA9E575C18A','none','trinh','le','2017-12-11 05:48:55',NULL,NULL,'tr@gmail.com','0',NULL,NULL,NULL),(7,'Qqqq','*734BB57BE5AE629BD42B31136E5D96821B02275C','none','Yyyy','Rrrr','2017-12-11 15:00:22',NULL,NULL,'Fff','0',NULL,NULL,NULL),(8,'test1','*9F6BC0ACD66A76DA54DC51E104F875A17F871EB3','none','duc','phan','2017-12-11 18:40:32',NULL,NULL,'a@gmail.com','0',NULL,NULL,NULL),(9,'test2','*6691484EA6B50DDDE1926A220DA01FA9E575C18A','none','test1','phan','2017-12-11 21:11:52',NULL,NULL,'aefwa','0',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-05 12:13:12
