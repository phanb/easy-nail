var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('../config.json');
var session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);
var sessionStore = new MySQLStore(config.database);


const SECRET = config.session.secret;

var forgot = require('./routes/forgot');
var reset = require('./routes/reset');

var app = express();
app.use(session({
    key: config.session.name,
    secret: config.session.secret,
    store: sessionStore,
    resave: true,
    saveUninitialized: true
}));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/forgot', forgot);
app.use('/reset', reset);


app.get('/', function(req, res, next) {
	if(res.session){
		res.redirect('/admin');

	}else{
 		res.render('index', { title: 'Easy Nail POS',user:req.session.user} );
	}
});

app.get('/test', function(req, res, next) {
	  res.setHeader('Access-Control-Allow-Origin', 'http://192.168.1.112:100009');
 		res.render('test', { message: 'Please login!'} );

});


app.get('/s/:store_id', function(req, res, next) {
				var store_id = req.params.store_id;
				const data = {mess:"will get it form graph"};
				res.render(__dirname+'/views/templates/1/index.jade',  {data} );
});
app.get('/x/:store_id', function(req, res, next) {
				console.log("at x store id");
				console.log(req.session.store_id);
				var store_id = req.params.store_id;
				req.session.store_id =  req.params.store_id;
				const data = {mess:"data of store will go here:"};
				res.render('checkin',  {data} );
});

app.get('/auth/logout',function(req,res){
req.session.destroy();
res.redirect('/');
});

app.get('/admin',function(req,res,next){
	if(!req.session.user){
 		res.render('admin', { message: 'Please login!'} );
		return;
	}
 			res.render('admin', { title: 'welcome to nail',user:req.session.user} );
});
app.get('/tech/:store_id',function(req,res,next){
	var store_id = req.params.store_id;
	if(!req.session.user){
 		res.render('index', { message: 'Please login!'} );
		return;
	}
 			res.render('admin_tech',{user:req.session.user,store_id:store_id});
});
app.get('/service/:store_id',function(req,res,next){
	var store_id = req.params.store_id;
	if(!req.session.user){
		res.send(401)
	}
	if(!store_id){
		res.status(401).send("missing store_id");
	}
 			res.render('admin_service',{user:req.session.user,store_id:store_id});
});
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  console.log("req",req.connection.remoteAddress);
  var err = new Error('Not Found! KEEP LOOKING !!!');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
module.exports = app;
