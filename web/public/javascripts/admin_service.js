app=angular.module('app', []);
app.controller('admin_service', function($scope, $filter, $http) {

$scope.mutationRefresh=x=>{
//send data to gg and refresh page if ok, else alert error;
     $http({ method : "POST", url : "/gg", data:{query:x},
    }).then(function mySucces(response) {
			window.location.reload();
    }, function myError(response) {
        console.log("error!!!");
    });
};


$scope.get_service=(store_id)=>{
const data = `{ getStores(store_id:${store_id}){ items { item_id name name_vn store_id price description1 description2 description3 } } }`;
         $http({
        method : "POST",
        url : "/gg",
				data:{query:data}
    }).then(function mySucces(response) {
        $scope.data = response.data.data.getStores[0].items;
    }, function myError(response) {
        console.log("error!!!");
    });
}
$scope.op_add_service =()=>{
$scope.isLoadEdit = false;

}
$scope.remove_service=function(salon){

	const data = `mutation{ removeItem(store_id:${salon.store_id},item_id:${salon.item_id}) }`;
$scope.mutationRefresh(data);

 };
//end of delete_employee


$scope.edit_service=()=>{
const data = `
mutation{
  editItem(
    item_id     :${$scope.item_id},       
	  store_id    :${$scope.store_id},        
		name        :"${$scope.name}",   
		name_vn     :"${$scope.name_vn}",         
		description1:"${$scope.description1}", 
		description2:"${$scope.description2}", 
		description3:"${$scope.description3}", 
		price       :${$scope.price}, 
	) 
}`;
$scope.mutationRefresh(data);
}

$scope.add_service=function(store_id){
const data = `
mutation{
addItem(
	name:"${ $scope.name}",
	name_vn:"${ $scope.name_vn}",
	description1:"${ $scope.description1}",
	description2:"${ $scope.description2}",
	description3:"${ $scope.description3}",
	price:${$scope.price},
	store_id: ${window.store_id},
){item_id}
}
`;
$scope.mutationRefresh(data);

};

$scope.get_service(window.store_id);
})

