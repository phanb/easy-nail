var localVideo;
var remoteVideo;
var peerConnection;
var room ="123";

var peerConnectionConfig = {
    'iceServers': [
        {'urls': 'stun:173.66.161.180:3478',credential:'magi',username:'bayaz', realm:"karaforfun.com"},
        {"url":"turn:173.66.161.180:3478", "credential":"magi","username":'bayaz',realm:"karaforfun.com"}
//        {'urls': 'stun:stun.services.mozilla.com'},
//        {'urls': 'stun:stun.l.google.com:19302'},
    ]
};

function pageReady() {

    localVideo = document.getElementById('localVideo');
    remoteVideo = document.getElementById('remoteVideo');
    socket = io();
    socket.on('news', function (data) {
    gotMessageFromServer(data);
   // console.log(data);
  });

//    serverConnection = new WebSocket('wss://' + window.location.hostname + ':8443');
//    serverConnection.onmessage = gotMessageFromServer;

    var constraints = {
        video: true,
        audio: true,
    };

    if(navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia(constraints).then(getUserMediaSuccess).catch(errorHandler);
    } else {
        alert('Your browser does not support getUserMedia API');
    }
}

function getUserMediaSuccess(stream) {
    localStream = stream;
    localVideo.src = window.URL.createObjectURL(stream);
}

function killall(){
	socket.emit('news',JSON.stringify({'ice': event.candidate, id:socket.id,room:room,killall:1}));
}
function start(isCaller) {
    peerConnection = new RTCPeerConnection(peerConnectionConfig);
    peerConnection.onicecandidate = gotIceCandidate;
    peerConnection.onaddstream = gotRemoteStream;
    peerConnection.addStream(localStream);
 
    console.debug("peerConnection:");
    console.debug(peerConnection);
    if(isCaller) {
        peerConnection.createOffer().then(createdDescription).catch(errorHandler);
    }
}

function gotMessageFromServer(message) {
	
    if(!peerConnection) start(false);

    var signal = JSON.parse(message);
    if(!signal.room){return;}

    // Ignore messages from ourself
    if(signal.id == socket.id) {console.debug("FOUND MYSELF");return;}
	console.debug(signal);
    if(signal.sdp) {
        peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp)).then(function() {
            // Only create answers in response to offers
            if(signal.sdp.type == 'offer') {
                peerConnection.createAnswer().then(createdDescription).catch(errorHandler);
            }
        }).catch(errorHandler);
    } else if(signal.ice) {
        peerConnection.addIceCandidate(new RTCIceCandidate(signal.ice)).catch(errorHandler);
    }
}

function gotIceCandidate(event) {
    if(event.candidate != null) {
//        serverConnection.send(JSON.stringify({'ice': event.candidate, 'uuid': uuid}));
	socket.emit('news',JSON.stringify({'ice': event.candidate, id:socket.id,room:room}));
	
    }
}

function createdDescription(description) {
    console.log('got description');

    peerConnection.setLocalDescription(description).then(function() {
//        serverConnection.send(JSON.stringify({'sdp': peerConnection.localDescription, 'uuid': uuid}));
	
socket.emit('news', JSON.stringify({'sdp': peerConnection.localDescription, id:socket.id,room:room}));
    }).catch(errorHandler);
}

function gotRemoteStream(event) {
    console.log('got remote stream');
    remoteVideo.src = window.URL.createObjectURL(event.stream);
}

function errorHandler(error) {
    console.log(error);
}


