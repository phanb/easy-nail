app.controller('reset_pass', function($scope, $filter, $http){

	$scope.isUpdate = false;

	$scope.pwdConfirm = function(){
		//var pwdc = document.getElementById('pwd_confirm_msg');
		if($scope.confirm_password === $scope.password){
			//pwdc.innerHTML = "Matched";
			$scope.isPwd = true;
			$scope.isUpdate = true;
		}
		else{
			//pwdc.innerHTML = "Not Match";
			$scope.isPwd = false;
			$scope.isUpdate = false;
		}
	};//pwdconfirm

	$scope.pwdCheck = function(){
		$scope.isPwd =  false;
		//var pwdm = document.getElementById('pwd_msg');
		var p = $scope.password;
    var anUpperCase = /[A-Z]/;
    var aLowerCase = /[a-z]/; 
    var aNumber = /[0-9]/;
    var aSpecial = /[!|@|#|$|%|^|&|*|(|)|-|_]/;

    var obj = {};
    obj.result = true;

    if(p < 8){
			$scope.isPwd = false;
			//pwdm.innerHTML ="Mininum is 8 characters";
    }
		else{

			var numUpper = 0;
			var numLower = 0;
			var numNums = 0;
			var numSpecials = 0;
			for(var i=0; i<p.length; i++){
				if(anUpperCase.test(p[i]))
						numUpper++;
				else if(aLowerCase.test(p[i]))
						numLower++;
				else if(aNumber.test(p[i]))
						numNums++;
				else if(aSpecial.test(p[i]))
						numSpecials++;
			}
			$scope.numUp = (numUpper >= 1 ? true: false);
			$scope.numLow = (numLower >= 1 ? true: false);
			$scope.numNum = (numNums >= 1 ? true: false);
			$scope.numSpec = (numSpecials >= 1 ? true: false);

			if(numUpper < 1 || numLower < 1 || numNums < 1 || numSpecials < 1){
				$scope.isPwd = false;
				//pwdm.innerHTML = "At least one Upper Case, one lower case, one number, one special";
			}
			else{
				$scope.isPwd = true;
				//pwdm.innerHTML = "OK";
			}
		}
	};//pwdcheck

	$scope.updatePass = function(){
		console.debug('update pass');
		data={
		 password: $scope.password,
		};
		console.log(data);
		$http({
				method: 'post',
				url: location.pathname,
				data:data,
				headers: {
						'Content-type': 'application/json;charset=utf-8'
				}
		})
		.then(function(response) {
			alert("Thank you for registering with us!\n Your account is ready, please login!");
			window.location= '/';
		}, function(rejection) {
				console.log(rejection.data);
		});

	};//updatePass

});//app
