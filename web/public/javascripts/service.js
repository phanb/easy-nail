app=angular.module('app', []);
app.controller('employees', function($scope, $filter, $http) {
$scope.delete_service=function(store_id,item_id){
	$http({
	    method: 'DELETE',
	    url: '/admin/service/?item_id=' + item_id + "&store_id="+store_id,
	    headers: {
	        'Content-type': 'application/json;charset=utf-8'
	    }
	})
	.then(function(response) {
		window.location.reload();
		
	}, function(rejection) {
	    console.log(rejection.data);
	});
};//end of delete_employee

$scope.add_service=function(store_id){
	data={
	name: $scope.name,
	name_nv: $scope.name_vn,
	description1: $scope.description1,
	description2: $scope.description2,
	description3: $scope.description3,
	price:$scope.price,
	store_id: document.getElementById("store_id").value,
	
	};
	
	$http({
	    method: 'put',
	    url: '/admin/service',
	    data:data,
	    headers: {
	        'Content-type': 'application/json;charset=utf-8'
	    }
	})
	.then(function(response) {
		window.location.reload();
	}, function(rejection) {
	    console.log(rejection.data);
	});
};//end of delete_employee


})

