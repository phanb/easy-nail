//app=angular.module('app', []);
app.controller('admin_tech', function($scope, $filter, $http) {

$scope.mutationRefresh=x=>{
//send data to gg and refresh page if ok, else alert error;
     $http({ method : "POST", url : "/gg", data:{query:x},
    }).then(function mySucces(response) {
			window.location.reload();
    }, function myError(response) {
        console.log("error!!!");
    });
};

$scope.getTeches=(store_id)=>{
	const data = `{ getStores(store_id:${store_id}){ teches { user_name first_name last_name password phone email hours avartar_path created_by created_at tech_id} } }`;
         $http({
        method : "POST",
        url : "/gg",
				data:{query:data},
    }).then(function mySucces(response) {
        $scope.data = response.data.data.getStores[0].teches;
    }, function myError(response) {
        console.log("error!!!");
    });
};


$scope.checkTechPhone=()=>{
	if($scope.phone.length < 10) return;
	$scope.isPhone = false;
	if(Object.keys($scope.currTech).length != 0){
	  if($scope.currTech.phone == $scope.phone){
	  	return;
	  }
	}
	const data = `{
	  getTech(filter:{
		  store_id:${window.store_id}, 
		  phone: "${$scope.phone}"
		}){user_name}
	   } `;
    $http({
        method : "POST",
        url : "/gg",
		data:{query:data},
    }).then(function mySucces(response) {
		if(response.data.data.getTech == null){
		  $scope.isPhone = false;
		}
		else{
		  $scope.isPhone = true;
		}
    }, function myError(response) {
        console.log("error!!!");
    });
};//checkTechPhone

$scope.emailValidator=(email)=>{
	  if(!email) return false;
	  let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  return re.test(email.toLowerCase());
};//emailValidator

$scope.checkTechEmail=()=>{
	$scope.isEmail = false;
	$scope.em = $scope.emailValidator($scope.email);
	if(!$scope.em){
	  return
	}
	if(Object.keys($scope.currTech).length != 0){
	  if($scope.currTech.email == $scope.email){
	  	return;
	  }
	}
	const data = `{
	  getTech(filter:{
		  store_id:${window.store_id}, 
		  email: "${$scope.email}"
		}){user_name email}
	   } `;
    $http({
        method : "POST",
        url : "/gg",
		data:{query:data},
    }).then(function mySucces(response) {
		if(response.data.data.getTech == null){
		  $scope.isEmail = false;
		}
		else{
		  $scope.isEmail = true;
		}
    }, function myError(response) {
        console.log("error!!!");
    });
};//checkTechEmail

$scope.checkTechUserName=()=>{
	$scope.isUserName = false;
	const data = `{
	  getTech(filter:{
		  store_id:${window.store_id}, 
		  user_name: "${$scope.user_name}"
		}){user_name}
	   } `;
    $http({
        method : "POST",
        url : "/gg",
		data:{query:data},
    }).then(function mySucces(response) {
		if(response.data.data.getTech == null){
		  $scope.isUserName = false;
		}
		else{
		  $scope.isUserName = true;
		}
    }, function myError(response) {
        console.log("error!!!");
    });
};

$scope.remove_tech=tech=>{
	let r = confirm('Are you sure?');
	if(r == false) return;
	const data = ` mutation{ removeTech( store_id:${window.store_id}, tech_id:${tech.tech_id}) }`;
	$scope.mutationRefresh(data);
}

$scope.check_username=()=>{
  const data = `mutation{
	checkUsername(
	  user_name: "${$scope.user_name}"
	)}`;
  $scope.mutationRefresh(data);
}//check_username

$scope.reset_tech=()=>{
  $scope.first_name = null;
  $scope.last_name = null;
  $scope.email = null;
  $scope.phone = null;
  $scope.user_name = null;
  $scope.password = null;
};//reset_tech

$scope.add_tech=()=>{
  //wrong email format
  if($scope.email && !$scope.isEmail && !$scope.em){
	return;
  }

  //some is taken
  if($scope.isUserName || $scope.isPhone || $scope.isEmail){
	return //wrong username and phone
  }

		const data = `mutation{
			addTech(
   			first_name :"${$scope.first_name}" ,
   			last_name  :"${$scope.last_name}" ,
   			email      :"${$scope.email}" ,
   			phone      :"${$scope.phone}" ,
   			user_name  :"${$scope.user_name}" ,
   			password   :"${$scope.password}" ,
   			store_id : ${store_id}
			){
			tech_id
			}
		}`;
	$scope.mutationRefresh(data);
}

$scope.edit_tech = ()=>{
  //wrong email format
  if($scope.email && !$scope.isEmail && !$scope.em){
	return;
  }

  //some is taken
  if($scope.isUserName || $scope.isPhone || $scope.isEmail){
	return //wrong username and phone
  }
	let password = "";
	if($scope.edit_password !=='xxxx'){
		password= `password: "${$scope.edit_password}",`;
	 }
	const data = `
	mutation{
  editTech(
   store_id : ${window.store_id},
   tech_id :   ${$scope.edit_tech_id},
   first_name :"${$scope.first_name}",
   last_name  :"${$scope.last_name}",
   email      :"${$scope.email}",
   phone      :"${$scope.phone}",
   user_name  :"${$scope.user_name}",
	 ${password}
) } `;

	$scope.mutationRefresh(data);
}


$scope.currTech = {};

$scope.load_tech = (tech)=>{
	$scope.currTech = tech;
    $scope.first_name =tech.first_name;
    $scope.last_name  =tech.last_name ;
    $scope.email      =tech.email     ;
    $scope.phone      =tech.phone    ;
    $scope.user_name  =tech.user_name ;
    $scope.edit_tech_id   = tech.tech_id;
    $scope.password   ='xxxx';
}

/*
//simplify
$scope.load_tech = (tech)=>{
    $scope.edit_first_name =tech.first_name;
    $scope.edit_last_name  =tech.last_name ;
    $scope.edit_email      =tech.email     ;
    $scope.edit_phone      =tech.phone    ;
    $scope.edit_user_name  =tech.user_name ;
    $scope.edit_tech_id   = tech.tech_id;
    $scope.edit_password   ='xxxx';
}
*/

$scope.getTeches(window.store_id);
})


//source:https://codepen.io/vladymy/pen/oboEBo

app.directive('phoneInput', function($filter, $browser) {
    return {
        require: 'ngModel',
        link: function($scope, $element, $attrs, ngModelCtrl) {
            var listener = function() {
                var value = $element.val().replace(/[^0-9]/g, '');
                $element.val($filter('tel')(value, false));
            };

            // This runs when we update the text field
            ngModelCtrl.$parsers.push(function(viewValue) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
            });

            // This runs when the model gets updated on the scope directly and keeps our view in sync
            ngModelCtrl.$render = function() {
                $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function(event) {
                var key = event.keyCode;
                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                // This lets us support copy and paste too
                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
                    return;
                }
                $browser.defer(listener); // Have to do this or changes don't get picked up properly
            });

            $element.bind('paste cut', function() {
                $browser.defer(listener);
            });
        }

    };
});
app.filter('tel', function () {
    return function (tel) {
        //console.log(tel);
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 1:
            case 2:
            case 3:
                city = value;
                break;

            default:
                city = value.slice(0, 3);
                number = value.slice(3);
        }

        if(number){
            if(number.length>3){
                number = number.slice(0, 3) + '-' + number.slice(3,7);
            }
            else{
                number = number;
            }

            return ("(" + city + ") " + number).trim();
        }
        else{
            return "(" + city;
        }

    };
});
