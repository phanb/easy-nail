app=angular.module('app', []);
app.controller('salon', function($scope, $filter, $http) {
$scope.getStores=function(){
				const data = `{ getStores { store_id name name2 address1 address2 city state zip phone hours } }`;	
        $http({
        method : "POST",
        url : "/gg",
				data:{query:data},
    }).then(function mySucces(response) {
        $scope.data = response.data.data.getStores;
    }, function myError(response) {
        console.log("error!!!");
    });
};

$scope.edit_salon=(salon)=>{
    $scope.edit_phone    = salon.phone;
    $scope.edit_name    = salon.name;
    $scope.edit_name2    = salon.name2;
    $scope.edit_address1 = salon.address1;
    $scope.edit_address2 = salon.address2;
    $scope.edit_city     = salon.city;
    $scope.edit_zip      = salon.zip;
    $scope.edit_state      = salon.state;
    $scope.store_id      = salon.store_id;

}
$scope.save_salon=()=>{
const data = `
		mutation{
		  editStore(
			store_id: ${$scope.store_id},
		  phone   :"${$scope.edit_phone}",
		  name    :"${$scope.edit_name}",
		  name2   :"${$scope.edit_name2}",
		  address1:"${$scope.edit_address1}",
		  address2:"${$scope.edit_address2}",
		  city    :"${$scope.edit_city}",
		  state    :"${$scope.edit_state}",
		  zip     :"${$scope.edit_zip}",
			) 
		
		}`;
	$http({
	    method: 'post',
	    url: 'gg',
	    data:{query:data},
	    headers: {
	        'Content-type': 'application/json;charset=utf-8'
	    }
	})
	.then(function(response) {
		window.location.reload();
	}, function(rejection) {
	    console.log(rejection.data);
	});

}

$scope.getStores();

$scope.build_teches=function(){
         $http({
        method : "GET",
        url : "/tech/"+window.store_id
    }).then(function mySucces(response) {
        $scope.data = response.data.pop();

        console.log($scope.data.user_stores[0].stores);
        console.log($scope.data.user_stores[0].stores[0].teches);
        console.log($scope.data.user_stores[0].stores[0].items);
    }, function myError(response) {
        console.log("error!!!");
    });
};


$scope.remove_salon=function(salon){
	$http({
	    method: 'POST',
	    url: '/gg',
			data: {query: `mutation{ removeStore(store_id:${salon.store_id}) }`}, 
			headers: {
	        'Content-type': 'application/json;charset=utf-8'
	    }
	})
	.then(function(response) {
		window.location.reload();
	}, function(rejection) {
	    console.log(rejection.data);
	});

};

$scope.create_salon=function(){

const data=`
mutation{
addStore(
		name: "${$scope.name}",
		name2: "${$scope.name2}",
		address1: "${$scope.address1}",
		address2: "${$scope.address2}",
		city: "${$scope.city}",
		state: "${$scope.state}",
		zip: "${$scope.zip}",
		phone: "${$scope.phone}",
		hours: "${$scope.hours}",
){
  store_id
  
}
}`;
	
$http({
	    url: '/gg',
			method: 'POST',
	    data:{query:data},
	    headers: {
	        'Content-type': 'application/json;charset=utf-8'
	    }
	})
	.then(function(response) {
		window.location.reload();
	}, function(rejection) {
	    console.log(rejection.data);
	});


};
$scope.check_name=function(){
console.log($scope.name);

}
$scope.process_form=function(){
	//do validation here
	$scope.add_store();
}
//end of add_store
//$scope.build_teches();


})


