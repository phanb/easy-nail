//app=angular.module('app', []);
app.controller('register', function($scope, $filter, $http) {
	
	$scope.isPhone = false;
	$scope.isUserName = false;
	$scope.isPwd = false;
	$scope.isConfirmPwd = false;
	$scope.isEmail = false;
	$scope.isFname = false;
	$scope.isLname = false;
	$scope.isregister = false;
  
  $scope.getRegister = function(){
	$scope.registerStatus();
	if($scope.isregister){
	  $scope.register();
	}	

  };

$scope.register=function(){
	var data={
	 last_name: $scope.last_name,
	 first_name: $scope.first_name,
	 user_name: $scope.user_name,
	 password: $scope.password,
	 email: $scope.email,
	 phone: $scope.phone,
	};
	//console.log(data);
	$http({
	    method: 'put',
	    url: '/gg/register',
	    data:data,
	    headers: {
	        'Content-type': 'application/json;charset=utf-8'
	    }
	})
	.then(function(response) {
		alert("Thank you for registering with us!\n Your account is ready, please login!");
		window.location.reload();
	}, function(rejection) {
//	    console.log(rejection.data);
	});

};//register

	$scope.userName = function(){
		$scope.isUserName = false;
//		console.debug($scope.user_name);
		//var um = document.getElementById('user_name_msg');
		//if($scope.user_name.length >= 6){
			//um.innerHTML = '';
			data={
				user_name: $scope.user_name,
			};
//			console.log(data);
			$http({
					method: 'post',
					url: '/gg/username',
					data:data,
					headers: {
							'Content-type': 'application/json;charset=utf-8'
					}
			})
			.then(function(response) {
//				console.debug('username ', response);
				if(response.data == 'ok'){
					//um.innerHTML = "OK";
					$scope.isUserName = true;
				}
				else{
					$scope.isUserName = false;
					//um.innerHTML = "Already taken";
				}	
				$scope.registerStatus();
			}, function(rejection) {
//					console.log(rejection.data);
					$scope.isUserName = false;
			});
		//}
		//else{
			//um.innerHTML = "minimum 6 characters";

		//}
	};//userName


	$scope.pwdConfirm = function(){
		if(($scope.confirm_password === $scope.password) && $scope.confirm_password.length >= 8){
		  $scope.isConfirmPwd = true;
		  $scope.registerStatus();
		}
		else{
		  $scope.isConfirmPwd = false;
		}
	};//pwdconfirm

	$scope.pwdCheck = function(){
//console.debug('pws check');
		//var pwdm = document.getElementById('pwd_msg');
		var p = $scope.password;
		var anUpperCase = /[A-Z]/;
		var aLowerCase = /[a-z]/; 
		var aNumber = /[0-9]/;
		var aSpecial = /[!|@|#|$|%|^|&|*|(|)|-|_]/;
		
		$scope.numUp = false;
		$scope.numLow = false;
		$scope.numNum = false;
		$scope.numSpec = false;
		
		var numUpper = 0;
		var numLower = 0;
		var numNums = 0;
		var numSpecials = 0;
		if(!p) return;
		for(var i=0; i<p.length; i++){
			if(anUpperCase.test(p[i]))
					numUpper++;
			else if(aLowerCase.test(p[i]))
					numLower++;
			else if(aNumber.test(p[i]))
					numNums++;
			else if(aSpecial.test(p[i]))
					numSpecials++;
		}
		if(numUpper >= 1){
		  $scope.numUp = true;
		}
		if(numLower >= 1){
		  $scope.numLow = true;
		}
		if(numNums >= 1){
		  $scope.numNum = true;
		}
		if(numSpecials >= 1){
		  $scope.numSpec = true;
		} 
	//	console.debug('pleng ',p.length);
		if($scope.numUp && $scope.numLow && $scope.numNum && $scope.numSpec && (p.length >= 8)){
		  $scope.isPwd = true;
		  $scope.registerStatus();
		}
		else{
		  $scope.isPwd = false;
		}
	};//pwdcheck

	$scope.emailValidator = function(email){
	  if(!email) return false;
	  let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  return re.test(email.toLowerCase());
	}//emailValidator

	$scope.emailCheck = function(){
		$scope.isEmail = false;
		var em = $scope.emailValidator($scope.email);
		if(em){
			data={
				email: $scope.email,
			};
		//	console.log(data);
			$http({
					method: 'post',
					url: '/gg/email',
					data:data,
					headers: {
							'Content-type': 'application/json;charset=utf-8'
					}
			})
			.then(function(response) {
				//console.debug('email ', response);
				if(response.data == 'ok'){
					$scope.isEmail = true;
				}
				else{
					$scope.isEmail = false;
				}	
				$scope.registerStatus();
			}, function(rejection) {
					console.log(rejection.data);
					$scope.isEmail = false;
			});
		}

	};//emailcheck
	
	$scope.phoneCheck = function(){
		//console.debug('phone ', $scope.phone);
		var phoneL = $scope.phone;
		//if(phoneL.toString().length >= 10){
			$scope.isPhone = true;
		//}
		$scope.registerStatus();
	};//phonecheck

	$scope.registerStatus = function(){
		//console.debug('registerStatus', $scope.first_name, $scope.last_name, 'email',$scope.isEmail, ' user ',$scope.isUserName, $scope.isPhone);
		if($scope.first_name && $scope.last_name)
		  if(($scope.first_name.length >= 2) && ($scope.last_name.length >= 2) && $scope.isEmail && $scope.isUserName && $scope.isConfirmPwd){
			$scope.isregister = true;
		  }
		  else{
			$scope.isregister = false;
		  }
		//console.debug('ok to reg ', $scope.isregister);
	};//registerStatus

})//app


