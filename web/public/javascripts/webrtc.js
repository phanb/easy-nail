var localVideo;
var remoteVideo;
var peerConnection;
var room ="123";


var audio_label = "FULL HD 1080P Webcam Analog Stereo";
var video_label = "FULL HD 1080P Webcam (0bda:58b0)"

var v_id = '20ed03929ae4972de4d60a3719adce6fe2ae6a4ff5695bb6b0457b6f4a835dde'
var a_id= '78929930823349a2741ddf008887239866cfc03c91a5e6d6bb147255cdfe1473'

var audio_input = null;
var video_input = null;


var peerConnectionConfig = {
    'iceServers': [
        //{'urls': 'stun:173.66.161.180:3478',credential:'magi',username:'bayaz', realm:"karaforfun.com"},
        {'urls': 'stun:stun.l.google.com:19302'},
        {"url":"turn:173.66.161.180:3478", "credential":"magi","username":'bayaz','realm':"karaforfun.com",'credentialType':'password'}
//        {'urls': 'stun:stun.services.mozilla.com'},
    ]
};
function gotDevices(deviceInfos) {

}

	

function pageReady() {
    localVideo = document.getElementById('localVideo');
    remoteVideo = document.getElementById('remoteVideo');
    navigator.mediaDevices.enumerateDevices().then(gotDevices).catch(errorHandler);
    socket = io();
    socket.on('news', function (data) {
    gotMessageFromServer(data);
   // console.log(data);
  });

//    serverConnection = new WebSocket('wss://' + window.location.hostname + ':8443');
//    serverConnection.onmessage = gotMessageFromServer;

    var constraints = {
	video:{
		width: { min: 100, ideal: 200 },
    		height: { min: 100 },
    		frameRate: 10,
    		facingMode: "user",
	},
        audio: true,
    };


    if(navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia(constraints).then(getUserMediaSuccess).catch(errorHandler);
    } else {
        alert('Your browser does not support getUserMedia API');
    }
}

function getUserMediaSuccess(stream) {
    localStream = stream;
    localVideo.src = window.URL.createObjectURL(stream);
}

function peak(){
	socket.emit('news',JSON.stringify({'ice': event.candidate, id:socket.id,room:room,peak:1}));
}
function killall(){
	socket.emit('news',JSON.stringify({'ice': event.candidate, id:socket.id,room:room,killall:1}));
}
function start(isCaller) {

    peerConnection = new RTCPeerConnection(peerConnectionConfig);
    peerConnection.onicecandidate = gotIceCandidate;
    peerConnection.onaddstream = gotRemoteStream;
    peerConnection.addStream(localStream);
 
    console.debug("peerConnection:");
    console.debug(peerConnection);
    if(isCaller) {
        peerConnection.createOffer().then(createdDescription).catch(errorHandler);
    }
}

function gotMessageFromServer(message) {
	
    if(!peerConnection) start(false);

    var signal = JSON.parse(message);
    if(!signal.room){return;}

    // Ignore messages from ourself
    if(signal.killall){ window.location = "http://google.com";}
    if(signal.id == socket.id) {console.debug("FOUND MYSELF");return;}
	console.debug(signal);
    if(signal.sdp) {
        peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp)).then(function() {
            // Only create answers in response to offers
            if(signal.sdp.type == 'offer') {
                peerConnection.createAnswer().then(createdDescription).catch(errorHandler);
            }
        }).catch(errorHandler);
    } else if(signal.ice) {
        peerConnection.addIceCandidate(new RTCIceCandidate(signal.ice)).catch(errorHandler);
    }
}

function gotIceCandidate(event) {
    if(event.candidate != null) {
//        serverConnection.send(JSON.stringify({'ice': event.candidate, 'uuid': uuid}));
	socket.emit('news',JSON.stringify({'ice': event.candidate, id:socket.id,room:room}));
	
    }
}

function createdDescription(description) {
    console.log('got description');

    peerConnection.setLocalDescription(description).then(function() {
//        serverConnection.send(JSON.stringify({'sdp': peerConnection.localDescription, 'uuid': uuid}));
	
socket.emit('news', JSON.stringify({'sdp': peerConnection.localDescription, id:socket.id,room:room}));
    }).catch(errorHandler);
}

function gotRemoteStream(event) {
    console.log('got remote stream');
    remoteVideo.src = window.URL.createObjectURL(event.stream);
}

function errorHandler(error) {
    console.log(error);
}


