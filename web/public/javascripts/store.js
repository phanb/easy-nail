app=angular.module('app', []);
app.controller('store', function($scope, $filter, $http) {
alert("at store javascript");
	
$scope.register=function(){
	data={
	 last_name: $scope.last_name,
	 first_name: $scope.first_name,
	 user_name: $scope.user_name,
	 password: $scope.password,
	 email: $scope.email,
	 phone: $scope.phone,
	};
	console.log(data);
	$http({
	    method: 'put',
	    url: '/users/register',
	    data:data,
	    headers: {
	        'Content-type': 'application/json;charset=utf-8'
	    }
	})
	.then(function(response) {
		alert("Thank you for registering with us!\n Your account is ready, please login!");
		window.location.reload();
	}, function(rejection) {
	    console.log(rejection.data);
	});

};//register

	$scope.userName = function(){
		console.debug($scope.user_name);
		var um = document.getElementById('user_name_msg');
		if($scope.user_name.length >= 6){
			um.innerHTML = '';
			data={
				user_name: $scope.user_name,
			};
			console.log(data);
			$http({
					method: 'post',
					url: '/users/username',
					data:data,
					headers: {
							'Content-type': 'application/json;charset=utf-8'
					}
			})
			.then(function(response) {
				console.debug('username ', response);
				if(response.data == 'ok'){
					um.innerHTML = "OK";
					$scope.isUserName = true;
				}
				else{
					um.innerHTML = "Already taken";
				}	
			}, function(rejection) {
					console.log(rejection.data);
			});
		}
		else{
			um.innerHTML = "minimum 6 characters";

		}
	};//userName


	$scope.pwdConfirm = function(){
		var pwdc = document.getElementById('pwd_confirm_msg');
		if($scope.confirm_password === $scope.password){
			pwdc.innerHTML = "Matched";
		}
		else{
			pwdc.innerHTML = "Not Match";
		}
	};//pwdconfirm

	$scope.pwdCheck = function(){
		var pwdm = document.getElementById('pwd_msg');
		var p = $scope.password;
    var anUpperCase = /[A-Z]/;
    var aLowerCase = /[a-z]/; 
    var aNumber = /[0-9]/;
    var aSpecial = /[!|@|#|$|%|^|&|*|(|)|-|_]/;
    var obj = {};
    obj.result = true;

    if(p < 8){
			$scope.isPwd = false;
			pwdm.innerHTML ="Mininum is 8 characters";
    }
		else{

			var numUpper = 0;
			var numLower = 0;
			var numNums = 0;
			var numSpecials = 0;
			for(var i=0; i<p.length; i++){
				if(anUpperCase.test(p[i]))
						numUpper++;
				else if(aLowerCase.test(p[i]))
						numLower++;
				else if(aNumber.test(p[i]))
						numNums++;
				else if(aSpecial.test(p[i]))
						numSpecials++;
			}

			if(numUpper < 1 || numLower < 1 || numNums < 1 || numSpecials < 1){
				$scope.isPwd = false;
				pwdm.innerHTML = "At least one Upper Case, one lower case, one number, one special";
			}
			else{
				$scope.isPwd = true;
				pwdm.innerHTML = "OK";
			}
		}
	};//pwdcheck

	$scope.emailCheck = function(){
		console.debug($scope.email);
		var em = document.getElementById('email_msg');
		if($scope.email.length >= 6){
			em.innerHTML = '';
			data={
				email: $scope.email,
			};
			console.log(data);
			$http({
					method: 'post',
					url: '/users/email',
					data:data,
					headers: {
							'Content-type': 'application/json;charset=utf-8'
					}
			})
			.then(function(response) {
				console.debug('email ', response);
				if(response.data == 'ok'){
					em.innerHTML = "OK";
					$scope.isEmail = true;
				}
				else{
					em.innerHTML = "Already existed";
				}	
			}, function(rejection) {
					console.log(rejection.data);
			});
		}
		else{
			em.innerHTML = "not valid email";

		}

		$scope.registerStatus();
	};//emailcheck
	
	$scope.phoneCheck = function(){
		console.debug('phone ', $scope.phone);
		var phoneL = $scope.phone;
		//if(phoneL.toString().length >= 10){
			$scope.isPhone = true;
		//}
		$scope.registerStatus();
	};//phonecheck

	$scope.registerStatus = function(){
		//console.debug('registerStatus', $scope.first_name, $scope.last_name, $scope.isEmail, $scope.isUserName, $scope.isPhone);
		if(($scope.first_name.length >= 2) && ($scope.last_name.length >= 2) && $scope.isEmail && $scope.isUserName && $scope.isPhone){
			$scope.isregister = false;
		}
		else{
			$scope.isregister = true;
		}
	};//registerStatus

})

