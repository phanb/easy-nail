var express = require('express');
var router = express.Router();
var flash = require('express-flash');
var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var Client = require('node-rest-client').Client;
var client = new Client();

var RESET_EMAIL = 'aaplussolution@gmail.com';//testing email
var RESET_PASS = 'A@plus123';

router.use(flash());

router.get('/:token', function(req, res) {
  var args = {
    data: { token: req.params.token},
    headers: { "Content-Type": "application/json" }
  };
  //url with port #
  //var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
  
  //url without port
  var fullUrl = req.protocol + '://' + req.hostname + '/gg/reset';
  
  client.post(fullUrl, args, function (data, response) {
	if(data && (response.statusCode == 200)){
	  res.render('reset',{
		user: data
	  });
	}
	else{
      req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('/forgot');
	}
 });//client.post

});

router.post('/:token', function(req, res) {
  //console.log('web data ', req.body);
  async.waterfall([
    function(done) {
	  var args = {
		data: { 
			reset_token: req.params.token,
			password: req.body.password,
		  },
		headers: { "Content-Type": "application/json" }
	  };
	  //url with port #
	  //var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	  
	  //url without port
	  var fullUrl1 = req.protocol + '://' + req.hostname + '/gg/reset';
	  var fullUrl2 = req.protocol + '://' + req.hostname + '/gg/resetUpdate';
	  //console.log('fullurl ', fullUrl1, '\n', fullUrl2);
	  client.post(fullUrl2, args, function (user, response) {
		//console.log("/n/n/n xxxxxxxxxxxxxxx ",user);
		//console.log("/n/n response ",response.statusCode);
		if(!user && (response.statusCode == 401)){
		  req.flash('error', 'Password reset token is invalid or has expired.');
		  return res.redirect('back');
		}
		  var err = null;
		  done(err, user);
		});//client post to update user
		
	},
    function(user, done) {
      var smtpTransport = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
          user: RESET_EMAIL,
          pass: RESET_PASS
        }
      });
      var mailOptions = {
        to: user.email,
        from: 'passwordreset@demo.com',
        subject: 'Your password has been changed',
        text: 'Hello,\n\n' +
          'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        req.flash('success', 'Success! Your password has been changed.');
        done(err);
      });
    }
  ], function(err) {
    res.redirect('/');
  });
});


module.exports = router;
