var express = require('express');
var router = express.Router();
var flash = require('express-flash');
var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var Client = require('node-rest-client').Client;
var client = new Client();

var EMAIL = "aaplussolution@gmail.com";//"emailhere@gmail.com";
var PASSWORD = "A@plus123";//"passhere";

router.use(flash());
router.get('/',function(req,res,next){
	res.render('forgot', {
    user: req.user,
	message:req.flash('signupMessage')
  });
});


router.post('/', function(req, res, next) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
		//console.log('token ',token, ' \nerr ',err, '\nbuf ',buf);
        done(err, token);
      });
    },
    function(token, done) {
      //models.user.findOne({email: req.body.email}).then( function(err, user) {
	  var args = {
		data: { 
			email: req.body.email,
			reset_token: token,
			reset_expires: Date.now() + 3600000 // 1 hour
			
		  },
		headers: { "Content-Type": "application/json" }
	  };
	  //url with port #
	  //var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	  
	  //url without port
	  var fullUrl1 = req.protocol + '://' + req.hostname + '/gg/forgot';
	  var fullUrl2 = req.protocol + '://' + req.hostname + '/gg/forgot';
	  client.post(fullUrl2, args, function (user, response) {
		if(!user && (response.statusCode == 401)){
		  req.flash('error', 'No account with that email address exists.');
		  return res.redirect('/forgot');
		}
		  var err = null;
		  done(err, token, user);
		});//client post to update user


    },
    function(token, user, done) {
      var smtpTransport = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
          user: EMAIL,
          pass: PASSWORD,
        }
      });
      var mailOptions = {
        to: user.email,
        from: 'passwordreset@demo.com',
        subject: 'Password Reset',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
        done(err, 'done');
      });
    }
  ], function(err) {
    if (err) return next(err);
    res.redirect('/forgot');
  });
});


module.exports = router;
