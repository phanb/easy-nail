var express = require('express');
var qr = require('qr-image');

var router = express.Router();

var flash = require('express-flash');
var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');

var RESET_EMAIL = 'xxx@gmail.com';
var RESET_PASS = 'xxx';

router.use(flash());

router.post('/authen',function(req,res,next){
	var user_name=req.body.user_name;
	var password=req.body.password;
	password = models.createPass(password);
  console.log(user_name,password)
	models.user.find({ where:[{user_name:user_name},{password:password}]}).then(function(data){
				if(data){	
       		req.session.user = data;
		res.redirect("/admin");	
}else{
 		res.render('index', { message:"Wrong username or password!",title: 'Easy Nail POS',user:req.session.user} );

}
	});
});
router.put('/register',function(req,res,next){
	var password=req.body.password;
	password = models.createPass(password);
	req.body['password'] = password;
	console.log(req.body);
	models.user.create(req.body).then(function(data){
		res.status(200).send("added!")
	});
});


router.post('/username',function(req,res){
	console.log(req.body);
	var user_name = req.body.user_name;
	models.user.find({where:[{user_name:user_name}]}).then(function(data){
		if(data == null)
			res.status(200).send("ok");
		else
			res.status(200).send("existed");
	});
});

router.post('/email',function(req,res){
	console.log(req.body);
	var email = req.body.email;
	models.user.find({where:[{email:email}]}).then(function(data){
		if(data == null)
			res.status(200).send("ok");
		else
			res.status(200).send("existed");
	});
});

router.post('/forgot', function(req, res, next) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
				console.log('token ',token, ' \nerr ',err, '\nbuf ',buf);
        done(err, token);
      });
    },
    function(token, done) {
      //models.user.findOne({email: req.body.email}).then( function(err, user) {
      models.user.findOne({where:{email: req.body.email }}).then( function(user) {
				console.log('erro ', ' user ',user);
        if (!user) {
					console.log('error no user ');
          req.flash('error', 'No account with that email address exists.');
          return res.redirect('/forgot');
        }

        user.reset_token = token;
        user.reset_expires = Date.now() + 3600000; // 1 hour

        user.save().then(function(user) {
					console.log('errrr ',user);
					var err = null;
          done(err, token, user);
        });
      });
    },
    function(token, user, done) {
      var smtpTransport = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
          user: RESET_EMAIL,
          pass: RESET_PASS
        }
      });
      var mailOptions = {
        to: user.email,
        from: 'passwordreset@demo.com',
        subject: 'Password Reset',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
					'Your user name: ' + user.user_name + '\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
        done(err, 'done');
      });
    }
  ], function(err) {
    if (err) return next(err);
    res.redirect('/forgot');
  });
});


module.exports = router;

