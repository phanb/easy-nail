export default (sequelize, DataTypes) => {
  const item = sequelize.define('item', {
     	name: DataTypes.STRING,
    	name_vn: DataTypes.STRING,
   	  item_id:{ 
            type: DataTypes.INTEGER,
            primaryKey: true,
            unique: true,
            autoIncrement: true
	   },
   	 store_id:{ type:DataTypes.INTEGER},
	   price: DataTypes.DECIMAL,
	   description1: DataTypes.STRING,
     description2: DataTypes.STRING,
	   description3: DataTypes.STRING,
  },
	{
		underscore: true,
	  timestamps: false,
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  }

);
  return item;
};

