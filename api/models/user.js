export default (sequelize, DataTypes) => {
  const user = sequelize.define('user', {

      user_name: DataTypes.STRING,
       password: DataTypes.STRING,
         social: DataTypes.STRING,
     first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
     created_at: DataTypes.DATE,
     last_login: DataTypes.DATE,
last_login_from: DataTypes.STRING,
          email: DataTypes.STRING,
   is_activated: DataTypes.INTEGER,
activation_code: DataTypes.STRING,
    reset_token: DataTypes.STRING,
	reset_expires: DataTypes.DATE,
   	user_id:{ 
            type: DataTypes.INTEGER,
            primaryKey: true,
            unique: true,
            autoIncrement: true
	}

});



  return user;
};
