export default (sequelize, DataTypes) => {
  const store_users = sequelize.define('user_stores', {
      user_id: {type:DataTypes.INTEGER,primaryKey:true},
      store_id: {type:DataTypes.INTEGER,primaryKey:true},
},
{
	  timestamps: false,
		underscore: true,
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  }
);
  return store_users;
};


