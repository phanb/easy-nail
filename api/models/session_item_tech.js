export default (sequelize, DataTypes) => {
  const session_item_tech = sequelize.define('session_item_tech', {
      gsession_id:  {type:DataTypes.INTEGER,primaryKey:true},
      tech_id:  {type:DataTypes.INTEGER,primaryKey:true},
      item_id:  {type:DataTypes.INTEGER,primaryKey:true},
},
{
	  timestamps: true,
		underscore: true,
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  }
);
  return session_item_tech;
};

