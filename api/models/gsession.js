export default (sequelize, DataTypes) => {
  const gsession = sequelize.define('gsession', {
	   total: DataTypes.DECIMAL,
     first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      approved_code: DataTypes.STRING,
   	gsession_id:{ 
            type: DataTypes.INTEGER,
            primaryKey: true,
            unique: true,
            autoIncrement: true
						},
      store_id: {type:DataTypes.INTEGER,primaryKey:true},
  }
,{
	  timestamps: true,
		underscore: true,
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  }


);

  return gsession;
};

