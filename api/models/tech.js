export default (sequelize, DataTypes) => {
  const tech = sequelize.define('tech', {
        tech_id:{ 
            type: DataTypes.INTEGER,                                                         
            primaryKey: true,
            unique: true,
            autoIncrement: true
        },
       store_id:{ type:DataTypes.INTEGER},
      user_name: DataTypes.STRING,
     first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
       password: DataTypes.STRING,
          phone: DataTypes.STRING,
     	  email: DataTypes.STRING,
     	  hours: DataTypes.STRING,
   avartar_path: DataTypes.STRING,
     created_by: DataTypes.INTEGER,
     created_at: DataTypes.DATE,
  },
	{
		underscore: true,
	  timestamps: false,
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  }

);
  return tech;
};

