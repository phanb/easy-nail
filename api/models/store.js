export default (sequelize, DataTypes) => {
  const store = sequelize.define('store', {
    name: DataTypes.STRING,
    name2: DataTypes.STRING,
    address1: DataTypes.STRING,
    address2: DataTypes.STRING,
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    zip: DataTypes.STRING,
    phone: DataTypes.STRING,
    hours: DataTypes.STRING,
   	store_id:{ 
            type: DataTypes.INTEGER,
            primaryKey: true,
            unique: true,
            autoIncrement: true
						}
  });
  store.associate = (models) => {
			store.belongsToMany(models.user,{through:models.user_stores});
	}

  return store;
};

