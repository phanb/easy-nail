import Sequelize from 'sequelize';
var config = require('../config.json');

const sequelize = new Sequelize(
  config.database.database,
  config.database.user,
  config.database.password,
  {
    host: config.database.host,
    dialect: 'mysql',
  },
);

const db = {
  user: sequelize.import('./user'),
  user_stores: sequelize.import('./user_stores'),
  store: sequelize.import('./store'),
  item: sequelize.import('./item'),
  tech: sequelize.import('./tech'),
  gsession: sequelize.import('./gsession'),
  session_item_tech: sequelize.import('./session_item_tech'),
};

Object.keys(db).forEach((modelName) => {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.createPass=(password)=> sequelize.fn('password',password);
//db.store.belongsTo(db.store_users,{foreignKey:'store_id'});
db.user_stores.belongsTo(db.store,{foreignKey:'store_id'});
db.user_stores.belongsTo(db.user,{foreignKey:'user_id'});
//db.user_stores.hasMany(db.store,{  foreignKey: 'store_id' });
db.store.hasMany(db.item,{  foreignKey: 'store_id' });
db.store.hasMany(db.tech,{  foreignKey: 'store_id' });
db.gsession.hasMany(db.store,{ foreignKey: 'store_id'});
db.store.hasMany(db.gsession,{foreignKey: 'store_id'});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
