export default (sequelize, DataTypes) => {
  const user_stores = sequelize.define('user_stores', {
      store_id:  {type:DataTypes.INTEGER,primaryKey:true},
      user_id:  {type:DataTypes.INTEGER,primaryKey:true},
},
{
	  timestamps: false,
		underscore: true,
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  }
);
  return user_stores;
};


