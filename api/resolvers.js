import bcrypt from 'bcrypt';
import _ from 'lodash';
import jwt from 'jsonwebtoken';
import Sequelize from 'sequelize';
const {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLFloat,
  GraphQLEnumType,
  GraphQLNonNull,
  GraphQLInterfaceType
} = require('graphql')
const Op = Sequelize.Op;
export default {
	user:{
		stores: async  (parent, args, { models })=>{
		const data = await		models.user_stores.findAll(
						{
							where:{user_id:parent.user_id},
							include:[ {model:models.store }]
						}
			);
  	  let rt = [];
			data.forEach((function(element){
				rt.push( element.store.dataValues );
			}));
			return rt;
		}
	},
	store:{
		users: async  (parent, args, { models },info)=>{
		const data = await		models.user_stores.findAll(
						{
							where:{store_id:parent.store_id},
							include:[ {model:models.user }]
						}
			);
  	  let rt = [];
			data.forEach((function(element){
				rt.push(element.user.dataValues);
			}));
			return rt;
		},
		items: {
        args: [{
						name:'item_id',
            type :new GraphQLList(GraphQLInt),

				}],
        resolve: async (parent, args,{models},info)=>{
					args['store_id']=parent.store_id;
					return models.item.findAll({where:args});
        }
			
		},
		teches: (parent, args, { models })=>models.tech.findAll({where:{store_id: parent.store_id}}),
		openedSessions:{
			resolve:async  (parent, args, { models },info)=> models.gsession.findAll({where:{store_id: parent.store_id,approved_code:null}}),
		},
		closedSessions:{
			resolve:async  (parent, args, { models },info)=> models.gsession.findAll({where:{store_id: parent.store_id,approved_code:{[Op.ne]:null}}}),
		}
	},//end of store



  gsession:{
		session_item_techs: (parent, args, { models })=>models.session_item_tech.findAll({where:{gsession_id: parent.gsession_id}}),
		
	},
  session_item_tech:{
		tech: (parent, args, { models })=>models.tech.find({where:{tech_id: parent.tech_id}}),
		item: (parent, args, { models })=>models.item.find({where:{item_id: parent.item_id}}),
	},

  Query: {
		allStores:(parent,args, {models}) =>models.store.findAll(),
    allUsers: (parent, args, { models }) => models.user.findAll(),
    getStores: async (parent, args ,{ models , user }) =>{
				const data = await models.user_stores.findAll({
					where:({user_id:user.user_id}),
					include:[{model: models.store,where:args},]
				});
  	  let rt = [];
			data.forEach((function(element){
//			console.log("+++++++++++++++++++************\n",element.store.dataValues);
				rt.push(element.store.dataValues);
			}));
			return rt;
			
			a.forEach((function(element){
				console.log(element);
			}));
		return rt;
		},

	//check existing tech
	getTech:(parent, args, {models}) => {
	  const data = models.tech.findOne({
		where:args.filter
	  }); 
	  return data;
	},//getTech

  },

  Mutation: {
		
   	editTech:   async (parent,args,{ models,user })=>{
			//*** need to make sure the user is associate with the store.
				return await models.tech.update(args,{where:{tech_id:args.tech_id}});
		},
		removeTech: async (parent,args,{ models,user })=>{
				//need to verify user store tech here;
			return	models.tech.destroy({where:args});
		},
   	addTech:   async (parent,args,{ models,user })=>{
			//*** need to make sure the user is associate with the store.
				return await models.tech.create(args);
		},
		addGuestSession: async (parent,args,{ models,user })=>{
			//*** need to make sure the user is associate with the store.
			return  await models.gsession.create(args);
		},
		addSessionItem: async (parent,args,{ models,user })=>{
			//*** need to make sure the user is associate with the store.
			return  await models.session_item_tech.create(args);
		},
		
//    createUser: (parent, args, { models }) => models.User.create(args),
   	addItem:   async (parent,args,{ models,user })=>{
			//*** need to make sure the user is associate with the store.
			return  await models.item.create(args);
		},
   	editItem:   async (parent,args,{ models,user })=>{
			//*** need to make sure the user is associate with the store.
			return  await models.item.update(args,{where:{item_id:args.item_id}});
		},
   	removeItem:   async (parent,args,{ models,user })=>{
			//*** need to make sure the user is associate with the store.
			return  await models.item.destroy({where:{item_id:args.item_id}});
		},
		
    addStore: async (parent,args,{ models,user })=>{
			const new_store = await models.store.create(args);
			const q = await models.user_stores.create(
				{ store_id:new_store.store_id, user_id:user.user_id }
			);
			return new_store;
		},
    removeStore: async(parent,args,{ models,user })=>{ 
		await models.item.destroy({where:args});
		await models.user_stores.destroy({where:args});
		//need to check user store here
		return models.store.destroy({where:args});
		},
    editStore: async (parent,args,{ models,user })=>{
				return		 models.store.update(args,{where:{store_id:args.store_id}});
		},
    removeUser: (parent,args,{ models })=>models.user.destroy({where:args}),
    updateUser: (parent, { username, newUsername }, { models }) =>
    models.user.update({ username: newUsername }, { where: { username } }),
		userAddStore:(parent,args,{models})=>{
		return	models.user_stores.create(args);
		},

    deleteUser: (parent, args, { models }) =>
      models.user.destroy({ where: args }),

    register: async (parent,args,{models})=>{
		const user= args;
		user.password = await bcrypt.hash(user.password,12);
		return		models.user.create(user);
		},

    login: async (parent,{email,password},{models,SECRET})=>
		{
			const user = await models.user.findOne({where:{email}});
			if(!user){
					throw new Error(' NO email FOUND');	
			}
			const valid = await bcrypt.compare(password,user.password);
			if(!valid){
				throw new Error("Email Found, Password is wrong!");
			}
			const token = jwt.sign(
				{
					user: _.pick(user,['id','username'])			
			 },SECRET,{
			expiresIn: '1y',
		});
		return token;
		},

  },
};


