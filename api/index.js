import express from 'express';
import bodyParser from 'body-parser';
import { graphiqlExpress, graphqlExpress } from 'graphql-server-express';
import { makeExecutableSchema } from 'graphql-tools';
import jwt from 'jsonwebtoken';
import typeDefs from './schema';
import resolvers from './resolvers';
import models from './models';
import bcrypt from 'bcrypt';

import async from 'async';
import crypto from 'crypto';
import nodemailer from 'nodemailer';
import flash from 'express-flash';

//import forgot from './forgot';  where is forgot ???

var RESET_EMAIL = 'aaplussolution@gmail.com';//'emailHere@gmail.com';
var RESET_PASS = 'A@plus123';//'passwordHere';


var cookieParser = require('cookie-parser');
var config = require('./config.json');
var router = express.Router();
var session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);
var sessionStore = new MySQLStore(config.database);


const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

const SECRET = config.session.secret;

const app = express();
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser()); 

app.use(session({
    key: config.session.name,
    secret: config.session.secret,
    store: sessionStore,
    resave: true,
    saveUninitialized: true,
}));

app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser()); 

//app.use(forgot);
app.use(flash());
//app.use('/users', users);

app.post('/gg/username',function(req,res){
	var user_name = req.body.user_name;
	models.user.find({where:[{user_name:user_name}]}).then(function(data){
		if(data == null)
			res.status(200).send("ok");
		else
			res.status(200).send("existed");
	});
});

app.post('/gg/email',function(req,res){
	var email = req.body.email;
	models.user.find({where:[{email:email}]}).then(function(data){
		if(data == null)
			res.status(200).send("ok");
		else
			res.status(200).send("existed");
	});
});


app.put('/gg/register',function(req,res,next){
	var user = req.body;
	var password=user.password;
	password = models.createPass(password);
	user['password'] = password;
	models.user.create(user).then(function(data){
      var smtpTransport = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
          user: RESET_EMAIL,
          pass: RESET_PASS
        }
      });
      var mailOptions = {
        to: req.body.email,
        from: 'passwordreset@demo.com',
        subject: 'Create account with XXX',
        text: 'Hello,\n\n' +
          'This is a confirmation that the your account has just been created.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        req.flash('success', 'Success! Your account has been created.');
        done(err);
      });
	res.status(200).send("added!")
  });
});



app.get('/gg/authen',(req,res,next)=>{
	var username='ben';
	var password='abc';
	password = models.createPass(password);
	models.user.find({ where:[{user_name:username},{password:password}]}).then(function(data){
	if(data){	
	 		req.session.user = data;
			res.status(200).send("okey");
	}else{
			res.status(401).send("!");
	}
		});
	}

);
app.post('/gg/authen',(req,res,next)=>{
	var username=req.body.username;
	var password=req.body.password;
	password = models.createPass(password);
	models.user.find({ where:[{user_name:username},{password:password}]}).then(function(data){
	if(data){	
	 		req.session.user = data;
			res.status(200).send("okey");
	}else{
			res.status(401).send("!");
	}
		});
	}

);

//web backend call this to set reset user
app.post('/gg/reset',(req,res,next)=>{
	var token = req.body.token;
	models.user.findOne({where:{ reset_token: token, reset_expires: { $gt: Date.now() } }}).then( function(user) {
    if (!user) {
	  console.log('no user');
	  res.status(401).send('no user');
    }
	else{
	  res.status(200).send(user);
	}
  });

});//reset

app.post('/gg/resetUpdate',(req,res,next)=>{
	models.user.findOne({where:{reset_token: req.body.reset_token, reset_expires:{$gt: Date.now()} }}).then( function(user) {
    if (!user) {
	  //console.log('no user');
	  res.status(401).send('no user');
    }
	  user['password'] = models.createPass(req.body.password);
	  user['reset_token'] = null;
	  user['reset_expires'] = null;
	  user.save().then(function(user){
		res.status(200).send(user);
	  });
  });

});//resetUpdate

app.post('/gg/forgot',(req,res,next)=>{
	models.user.findOne({where:{email: req.body.email}}).then( function(user) {
	  if (!user) {
		//console.log('no user');
		res.status(401).send('no user');
	  }
	  else{
		user.reset_token = req.body.reset_token;
		user.reset_expires = req.body.reset_expires;
		user.save().then(function(user){
		  res.status(200).send(user);
		});
	  }
	});

});//forgot


app.use( '/gg/graphiql', graphiqlExpress({
    endpointURL: '/gg',
  }),
);

var checkUser = function(req,res,next){
	if(req.session.user){
		next();
	}else{
		res.status(401).send("NOT AUTHENTICATED!");
//next();
	}
}

app.post('/gg', checkUser,bodyParser.json(), graphqlExpress((req)=> {
  return ({ schema, context: {
    models,
    SECRET,
    user:req.session.user,
    } });
}),
);

//models.sequelize.sync().then(() => app.listen(4030));
models.sequelize.sync().then(() => app.listen(3030));
