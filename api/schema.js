export default `
	type store {
			name: String!
			name2: String
			address1:String
			address2: String
			city: String
			state: String
			zip: String
			phone:String
			hours:String
			store_id:Int!
			users:[user!]!
			items:[item]
			teches:[tech]
			gsessions:[gsession]
			openedSessions:[gsession]!
			closedSessions:[gsession]!
	}
  
  type item {
   	  item_id: Int!
     	name: String!
    	name_vn: String
   	 store_id:String
	   price: String
	   description1:String 
     description2: String
	   description3: String

	}
	
  type gsession {
   	  gsession_id: Int!
			first_name: String
			last_name: String
			approved_code:String
			store_id: String
			session_item_techs:[session_item_tech]

	}
  type session_item_tech {
			gsession_id: Int!
			tech_id     :Int!
			item_id    :Int!
      tech: tech
      item: item
	}
	
	type tech {
			tech_id     :Int!
			store_id    :Int!
			user_name   :String 
			first_name  :String 
			last_name   :String 
			password    :String 
			phone       :String 
			email       :String 
			hours       :String 
			avartar_path:String 
			created_by  :String 
			created_at  :String 
	}

  type user {
				user_id: Int!
	      user_name: String!
	      social: String
	     first_name: String
	     last_name: String
	     last_login_from: String
	     email: String
	     is_activated: Int
	     activation_code: String
	    reset_token: String
	    reset_expires: String
	    stores:[store]
  }

  type user_stores{
		store_id:Int!
		user_id: Int!
  }

  input TechFilter{
	store_id: Int!
	user_name: String
	email: String
	phone: String
  }

  type Query {
		allItems:[item!]!
		allStores: [store!]!
    allUsers: [user!]!
    getStores(store_id:[Int]):[store]
	getTech(filter: TechFilter!): tech
  }

  type Mutation {

		addGuestSession(
				store_id:Int!,
				first_name  : String
				last_name   : String
		):gsession!

		addSessionItem(
				store_id:Int!,
				tech_id: Int!,
				item_id: Int!,
				gsession_id: Int!,
		):session_item_tech!

		addTech(
				store_id:Int!,
				user_name   : String! 
				first_name  : String
				last_name   : String
				password    : String
				phone       : String 
				email       : String 
				hours       : String 
				avartar_path: String 
				created_by  : String 
				created_at  : String  
		):tech!

		editTech(
				store_id:Int!,
				tech_id:Int!,
				user_name   : String! 
				first_name  : String
				last_name   : String
				password    : String
				phone       : String 
				email       : String 
				hours       : String 
				avartar_path: String 
				created_by  : String 
				created_at  : String  
		):Int!
		removeTech(store_id:Int!,tech_id:Int!):Int!

    addItem(
				store_id:Int!,
				name:String!,
				name_vn:String,
				item_id:String,
				price:Float,
				description1:String,
				description2:String,
				description3:String,
		):item!

    editItem(
				store_id:Int!,
				item_id: Int!,
				name:String!,
				name_vn:String,
				price:Float,
				description1:String,
				description2:String,
				description3:String,
		):Int!
		
		removeItem(store_id:Int!,item_id:Int!):Int!

		addStore(
    	name: String!,
    	name2: String,
    	address1:String,
    	address2:String,
    	city: String,
    	state:String,
    	zip: String,
    	phone:String,
    	hours:String,
		):store!

		editStore(
			store_id: Int!,
    	name: String!,
    	name2: String,
    	address1:String,
    	address2:String,
    	city: String,
    	state:String,
    	zip: String,
    	phone:String,
    	hours:String,
		):[Int!]!
		
		removeStore(store_id:Int!): Int!



		removeUser(user_id:Int!): Int!
    userAddStore(userUserId:Int!,store_id:Int!):user_stores!
    updateUser(user_name: String!, newUsername: String!): [Int!]!
    deleteUser(user_name: String!): Int!
    register(user_name:String!,email:String!,password:String!):user!
    login(email:String!,password:String!): String!
  }
`;
